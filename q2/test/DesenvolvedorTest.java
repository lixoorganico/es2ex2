import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by lubuntu on 12/06/17.
 */
public class DesenvolvedorTest {

    private final int SALARIO_MIN = 2000;
    private final int SALARIO_BASE = 3000;
    private final int SALARIO_MAX = 5000;

    private final String DEFAULT_EMAIL = "email@mail.com";
    private final String DEFAULT_NOME = "Teste";

    @Test(expected = Exception.class)
    public void naoDeveTerNomeNulo() throws Exception {
        new Desenvolvedor(null, DEFAULT_EMAIL, SALARIO_MIN);
    }

    @Test(expected = Exception.class)
    public void naoDeveTerNomeVazio() throws Exception {
        new Desenvolvedor("", DEFAULT_EMAIL, SALARIO_MIN);
    }

    @Test(expected = Exception.class)
    public void naoDeveTerEmailNulo() throws Exception {
        new Desenvolvedor(DEFAULT_NOME, null, SALARIO_MIN);
    }

    @Test(expected = Exception.class)
    public void naoDeveTerEmailVazio() throws Exception {
        new Desenvolvedor(DEFAULT_NOME, "", SALARIO_MIN);
    }

    @Test
    public void deveTerSalario() throws Exception {
        Funcionario f = new Desenvolvedor(DEFAULT_NOME, DEFAULT_EMAIL, SALARIO_MIN);
        assertEquals(f.getNome(), DEFAULT_NOME);
        assertEquals(f.getEmail(), DEFAULT_EMAIL);
        assertEquals(f.getSalarioBase(), SALARIO_MIN, 0);

        f = new Desenvolvedor(DEFAULT_NOME, DEFAULT_EMAIL, SALARIO_MAX);
        assertEquals(f.getNome(), DEFAULT_NOME);
        assertEquals(f.getEmail(), DEFAULT_EMAIL);
        assertEquals(f.getSalarioBase(), SALARIO_MAX, 0);

        f = new Desenvolvedor(DEFAULT_NOME, DEFAULT_EMAIL, SALARIO_BASE);
        assertEquals(f.getNome(), DEFAULT_NOME);
        assertEquals(f.getEmail(), DEFAULT_EMAIL);
        assertEquals(f.getSalarioBase(), SALARIO_BASE, 0);
    }

    @Test(expected = Exception.class)
    public void naoDeveTerSalarioZerado() throws Exception {
        new Desenvolvedor(DEFAULT_NOME, DEFAULT_EMAIL, 0);
    }

    @Test(expected = Exception.class)
    public void naoDeveTerSalarioNegativo() throws Exception {
        new Desenvolvedor(DEFAULT_NOME, DEFAULT_EMAIL, -1);
    }

    @Test
    public void deveCalcularSalarioBase() {
        try {
            Funcionario f = new Desenvolvedor(DEFAULT_NOME, DEFAULT_EMAIL, SALARIO_MIN);
            assertEquals(TestUtils.desconta(f.getSalarioBase(), 10), f.getSalarioLiquido(), 0);

            f = new Desenvolvedor(DEFAULT_NOME, DEFAULT_EMAIL, SALARIO_BASE);
            assertEquals(TestUtils.desconta(f.getSalarioBase(), 20), f.getSalarioLiquido(), 0);

            f = new Desenvolvedor(DEFAULT_NOME, DEFAULT_EMAIL, SALARIO_MAX);
            assertEquals(TestUtils.desconta(f.getSalarioBase(), 20), f.getSalarioLiquido(), 0);
        } catch (Exception e) {
            fail();
        }
    }
}