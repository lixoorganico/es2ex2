import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by lubuntu on 12/06/17.
 */
public class DBATest {

    private final int SALARIO_MIN = 1000;
    private final int SALARIO_BASE = 2000;
    private final int SALARIO_MAX = 3000;

    private final String DEFAULT_EMAIL = "email@mail.com";
    private final String DEFAULT_NOME = "Teste";

    @Test(expected = Exception.class)
    public void naoDeveTerNomeNulo() throws Exception {
        new DBA(null, DEFAULT_EMAIL, SALARIO_MIN);
    }

    @Test(expected = Exception.class)
    public void naoDeveTerNomeVazio() throws Exception {
        new DBA("", DEFAULT_EMAIL, SALARIO_MIN);
    }

    @Test(expected = Exception.class)
    public void naoDeveTerEmailNulo() throws Exception {
        new DBA(DEFAULT_NOME, null, SALARIO_MIN);
    }

    @Test(expected = Exception.class)
    public void naoDeveTerEmailVazio() throws Exception {
        new DBA(DEFAULT_NOME, "", SALARIO_MIN);
    }

    @Test
    public void deveTerSalario() throws Exception {
        Funcionario f = new DBA(DEFAULT_NOME, DEFAULT_EMAIL, SALARIO_MIN);
        assertEquals(f.getNome(), DEFAULT_NOME);
        assertEquals(f.getEmail(), DEFAULT_EMAIL);
        assertEquals(f.getSalarioBase(), SALARIO_MIN, 0);

        f = new DBA(DEFAULT_NOME, DEFAULT_EMAIL, SALARIO_MAX);
        assertEquals(f.getNome(), DEFAULT_NOME);
        assertEquals(f.getEmail(), DEFAULT_EMAIL);
        assertEquals(f.getSalarioBase(), SALARIO_MAX, 0);

        f = new DBA(DEFAULT_NOME, DEFAULT_EMAIL, SALARIO_BASE);
        assertEquals(f.getNome(), DEFAULT_NOME);
        assertEquals(f.getEmail(), DEFAULT_EMAIL);
        assertEquals(f.getSalarioBase(), SALARIO_BASE, 0);
    }

    @Test(expected = Exception.class)
    public void naoDeveTerSalarioZerado() throws Exception {
        new DBA(DEFAULT_NOME, DEFAULT_EMAIL, 0);
    }

    @Test(expected = Exception.class)
    public void naoDeveTerSalarioNegativo() throws Exception {
        new DBA(DEFAULT_NOME, DEFAULT_EMAIL, -1);
    }

    @Test
    public void deveCalcularSalarioBase() {
        try {
            Funcionario f = new DBA(DEFAULT_NOME, DEFAULT_EMAIL, SALARIO_MIN);
            assertEquals(TestUtils.desconta(f.getSalarioBase(), 15), f.getSalarioLiquido(), 0);

            f = new DBA(DEFAULT_NOME, DEFAULT_EMAIL, SALARIO_BASE);
            assertEquals(TestUtils.desconta(f.getSalarioBase(), 25), f.getSalarioLiquido(), 0);

            f = new DBA(DEFAULT_NOME, DEFAULT_EMAIL, SALARIO_MAX);
            assertEquals(TestUtils.desconta(f.getSalarioBase(), 25), f.getSalarioLiquido(), 0);
        } catch (Exception e) {
            fail();
        }
    }
}