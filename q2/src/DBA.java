import com.sun.istack.internal.NotNull;

/**
 * Created by lubuntu on 12/06/17.
 */
public class DBA extends Funcionario {

    private final int BASE_SALARIAL = 2000;
    private final int DESCONTO_MAIS_BASE_SALARIAL = 25;
    private final int DESCONTO_MENOS_BASE_SALARIAL = 15;

    public DBA(@NotNull String nome, @NotNull String email, @NotNull double salarioBase) throws Exception {
        super(nome, email, salarioBase);
    }

    @Override
    public double getSalarioLiquido() {
        return getSalarioLiquido(getSalarioBase() >= BASE_SALARIAL ?
                DESCONTO_MAIS_BASE_SALARIAL : DESCONTO_MENOS_BASE_SALARIAL);
    }
}
