import com.sun.istack.internal.NotNull;

/**
 * Created by lubuntu on 12/06/17.
 */
public abstract class Funcionario {
    private String mNome;
    private String mEmail;
    private double mSalario;

    public Funcionario (@NotNull String nome, @NotNull String email, @NotNull double salario) throws Exception {
        setNome(nome);
        setEmail(email);
        setSalarioBase(salario);
    }

    public abstract double getSalarioLiquido();

    protected  double getSalarioLiquido (double desconto) {
        return mSalario - (mSalario * desconto);
    }

    public void setNome(String nome) throws Exception {
        if (nome == null || nome.trim() == "") {
            throw new Exception();
        }

        mNome = nome;
    }

    public void setEmail(String email) throws Exception {
        if (email == null || email.trim() == "") {
            throw new Exception();
        }

        mEmail = email;
    }

    public void setSalarioBase(double salario) throws Exception {
        if (salario <= 0) {
            throw new Exception();
        }

        mSalario = salario;
    }

    public String getNome() {
        return mNome;
    }

    public String getEmail() {
        return mEmail;
    }

    public double getSalarioBase() {
        return mSalario;
    }
}
