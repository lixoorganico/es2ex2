import com.sun.istack.internal.NotNull;

/**
 * Created by lubuntu on 12/06/17.
 */
public class Gerente extends Funcionario {

    private final int BASE_SALARIAL = 5000;
    private final int DESCONTO_MAIS_BASE_SALARIAL = 30;
    private final int DESCONTO_MENOS_BASE_SALARIAL = 20;

    public Gerente(@NotNull String nome, @NotNull String email, @NotNull double salarioBase) throws Exception {
        super(nome, email, salarioBase);
    }

    @Override
    public double getSalarioLiquido() {
        return getSalarioLiquido(getSalarioBase() >= BASE_SALARIAL ? DESCONTO_MAIS_BASE_SALARIAL : DESCONTO_MENOS_BASE_SALARIAL);
    }
}
