import com.sun.istack.internal.NotNull;

/**
 * Created by lubuntu on 12/06/17.
 */
public class Desenvolvedor extends Funcionario {

    private final int BASE_SALARIAL = 3000;
    private final int DESCONTO_MAIS_BASE_SALARIAL = 20;
    private final int DESCONTO_MENOS_BASE_SALARIAL = 10;

    public Desenvolvedor(@NotNull String nome, @NotNull String email, @NotNull double salarioBase) throws Exception {
        super(nome, email, salarioBase);
    }

    @Override
    public double getSalarioLiquido() {
        return getSalarioLiquido(getSalarioBase() >= BASE_SALARIAL ? DESCONTO_MAIS_BASE_SALARIAL : DESCONTO_MENOS_BASE_SALARIAL);
    }
}
