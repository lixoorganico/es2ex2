import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class FaturaTest {

	private final String DEFAULT_DATA = "01/01/2017";
    private final String DEFAULT_NOME = "Mario";
    

	
	@Test
	public void dataVazia() {
		Fatura f = new Fatura("", 500, DEFAULT_NOME);
		assertFalse(!f.getData().equals(""));
	}
	
	@Test
	public void nomeVazio() {
		Fatura f = new Fatura(DEFAULT_DATA, 500, "");
		assertFalse(!f.getNomeCliente().equals(""));
	}

}
