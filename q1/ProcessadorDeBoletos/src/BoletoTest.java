import static org.junit.Assert.*;

import org.junit.Test;

public class BoletoTest {

	private final String DEFAULT_DATA = "01/01/2017";
    private final String DEFAULT_NOME = "Mario";
    
    @Test
	public void valorNegativo() throws Exception {
		Boleto b = new Boleto(0, DEFAULT_DATA, -1);
		assertFalse(!(b.getValorPago() < 0));
    }
	

	
	@Test
	public void dataVazia() throws Exception {
		Boleto b = new Boleto(0, "", 500);
		assertFalse(!b.getData().equals(""));
	}

}
