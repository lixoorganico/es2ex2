import java.util.ArrayList;

public class Fatura {
	
	private double valorTotal;
	private String nomeCliente;
	private String data;
	
	public Fatura(String data, double valorTotal, String nomeCliente) {
		super();
		this.data = data;
		this.valorTotal = valorTotal;
		this.nomeCliente = nomeCliente;
	}
	
	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) throws Exception{
		
		if(nomeCliente == null || nomeCliente == ""){
			throw new Exception();
		}else{

			this.nomeCliente = nomeCliente;
		}
		
	}

	public String getData() {
		return data;
	}

	public void setData(String data) throws Exception{
		
		if(data == null || data == ""){
			throw new Exception();
		}else{
			this.data = data;
		}
		
	}
	
	public double somaBoletos(ArrayList<Boleto> listaDeBoleto) throws Exception{
		
		double valorTotal = 0;
		
		for (Boleto boleto : listaDeBoleto) {
			
			if(boleto.getValorPago() < 0){
				throw new Exception();
			}else{
				valorTotal += boleto.getValorPago();
			}			
			
		}
		return valorTotal;
	}
	
}
