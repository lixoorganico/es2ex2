import java.util.ArrayList;

public class ProcessadorDeBoleto {

	public static void main(String[] args) throws Exception {
		
		Fatura fatura = new Fatura("12/07/2017", 2000, "Mario");
		Boleto boleto1 = new Boleto(0, "12/07/2017", 500);
		Boleto boleto2 = new Boleto(1, "13/07/2017", 700);
		Boleto boleto3 = new Boleto(2, "14/07/2017", 300);
		Boleto boleto4 = new Boleto(3, "15/07/2017", 500);
		
		ArrayList<Boleto> listaDeBoletos = new ArrayList<Boleto>();
		listaDeBoletos.add(boleto1);
		listaDeBoletos.add(boleto2);
		listaDeBoletos.add(boleto3);
		listaDeBoletos.add(boleto4);
		
		if(fatura.somaBoletos(listaDeBoletos) >= fatura.getValorTotal()){
			System.out.println("Fatura de " + fatura.getValorTotal() + " com " + listaDeBoletos.size() + " boletos no valor de "
					+ boleto1.getValorPago() + ", " + boleto2.getValorPago() + ", " + boleto3.getValorPago() + " e " + boleto4.getValorPago() + ": fatura marcada como PAGA, "
					+ "e " + listaDeBoletos.size() + " pagamentos do tipo BOLETO criados.");
		}
				

	}	

}
